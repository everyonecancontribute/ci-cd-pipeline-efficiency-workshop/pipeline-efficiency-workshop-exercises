# Pipeline Efficiency Workshop Exercises

Pipeline Efficiency Workshop with GitLab CI/CD.

## Events

- [Open Source Automation Days: Efficient DevSecOps Pipelines in a cloud-native world](https://osad-munich.org/osad-workshops-2021/effiziente-devsecops-pipelines-in-einer-cloud-nativen-welt/) - [organisation issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4358)

## Resources

- [Slides with exercises](https://docs.google.com/presentation/d/12ifd_w7G492FHRaS9CXAXOGky20pEQuV-Qox8V4Rq8s/edit)
- [Files including solutions](https://gitlab.com/everyonecancontribute/ci-cd-pipeline-efficiency-workshop/pipeline-efficiency-workshop-exercises)
- [Documentation](https://docs.gitlab.com/)
- [Website](https://about.gitlab.com/)
- [Handbook](https://about.gitlab.com/handbook/)

# AWS Auto-Scaling with GitLab Runner

## Requirements

- AWS IAM account with credentials, configured in AWS CLI
- Terraform CLI
- GitLab CI/CD runner token

### Clone the module's example

```shell
$ git clone https://github.com/npalm/terraform-aws-gitlab-runner.git 
$ cd terraform-aws-gitlab-runner/examples/runner-default
```

Replace [main.tf](main.tf) and [variables.tf](variables.tf) from this repository.


### Additional Tasks

Get the Runner registration token from `Settings > CI/CD > Runners` in the group and add it as Terraform variables.

```shell
$ vim terraform.tfvars

registration_token = "..."
```

Optionally: Specify `gitlab_runner_version` variable with the most recent GitLab runner release.

## Provision

```shell

$ terraform init
$ terraform plan
$ terraform apply
```

## Destroy

```shell
$ terraform destroy
```
